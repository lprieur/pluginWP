<?php

/*
* Plugin Name: Mon premier plugin 
* Version: 1.0.0
* Author Name: Prieur Lorris
* Description: C'est un plugin de test
*/

/* function undefined_remove_menu_article(){
    //Permet de supprimer le menu article
    remove_menu_page('edit.php');
}
add_action('admin_menu','undefined_remove_menu_article', 10, 2 );

 */

include('create_table.php');
include('traitement.php');

class MonExemple {

    public function __construct() {
        // Mon code
        add_action('admin_menu', [$this, 'admin_add_menu']);
    }

    public function admin_add_menu() {
        add_menu_page(
            __('Mon titre', 'mon-titre'), // Titre de la page
            __('Mon menu', 'mon-menu'), //Libellé du menu
            'manage_options', //Capability
            'mon-menu',
            [$this, 'load_mon_menu']
        );
    }

    public function load_mon_menu() {?>
        <h1> Hello World ! </h1>
        <p> Mon texte de paragraphe : </p>
        <h2> Paramètre 1 </2>
        <form method="POST" action="">
                <div class="box">
                    <input type="file" />
                </div>
                <div class="box">
                    <input type="text" name="prix" placeholder="Prix"/> 
                </div>
                <div class="box">
                    <input type="text" name="reduc" placeholder="Réduction"/> 
                </div>
                <div class="box">
                    <input type="text" name="description" placeholder="Description du produit"/> 
                </div>
                <div class="box">
                    <input type="submit" value="Valider" />
                </div>
              </form> <?php

        /* input box : definir l'url de l'image du produit
        input box : definir le prix actuel
        input box : definir le prix apres remise
        zone de saisie pour mettre  un descript  */
    }

}

new MonExemple();
?>